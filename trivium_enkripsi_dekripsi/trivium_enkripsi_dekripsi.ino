

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
int status=0;
unsigned int kunci[80];
unsigned int plainteks[80];

unsigned int IV[80];
unsigned int S[288];

unsigned int t1,t2,t3;

void setup() {
  // initialize serial:
  Serial.begin(9600);

  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  Serial.print("Modul Enkripsi Trivium\n");
  Serial.print("Selamat Datang\n");
  Serial.print("Masukkan Kunci (10 karakter) : \n");
}

void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {
    //Serial.println(inputString);
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
/*void serialEvent1(){
  while (Serial.available()){
    char outChar = (char)Serial.print();
    
  }
}*/

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      switch(status){
      case 0:
      masukkunci();
      status=1;
      
      case 1:
      Serial.print("Masukkan IV (10 karakter) : \n");
      status=2;
      break;
      
      case 2:
      masukiv();
      Serial.print("Masukkan pesan : \n");
      status=3;
      break;
      
      case 3:
      masukpesan();
      status=4;
      
      case 4:
      inisial();
      status=5;
      
      case 5:
      persiapan();
      status=6;

      case 6:
      bangkit();
      status=7;
      
      default:
      status=7;
      ;
    }
      stringComplete = true;
    }
  }
}

void masukkunci(){
  Serial.print("kunci : " );
  Serial.print(inputString);
  inputString.trim();
  //String a="abcdefghij";
  int k=0;
  for (int i=0; i<inputString.length(); i++){
    char keyChar = inputString.charAt(i);
    for (int j=7; j>=0; j--){
      byte keybytes = bitRead(keyChar,j);
      kunci[k]=keybytes;
      k++;
    }
    //Serial.print(" ");
  }
  for(int i=0;i<80;i++){
    if(i%8==0){
      Serial.print(" "); 
    }
    if(i%32==0){
      Serial.print("\n");
    }
  Serial.print(kunci[i]);
  
  }
  Serial.print('\n');
  
}

void masukiv(){
  Serial.print("IV : " );
  Serial.println(inputString);
  inputString.trim();
   //String b="1234567890";
   int k=0;
  for (int i=0; i<inputString.length(); i++){
    char IvChar = inputString.charAt(i);
    for (int j=7; j>=0; j--){
      byte IVbytes = bitRead(IvChar,j);
      IV[k]=IVbytes;
      k++;
    }
  }
  for (int i=0; i<80; i++){
    if(i%8==0){
      Serial.print(" "); 
    }
    if(i%32==0){
      Serial.print("\n");
    }
    Serial.print(IV[i]);
  }
  Serial.print('\n');
  }

void masukpesan(){
          Serial.println(inputString);
          inputString.trim();
          int k=0;
          Serial.print("bitpesan = \n");
          for (int i=0; i<inputString.length(); i++){
            char plainChar = inputString.charAt(i);
            for (int j=7; j>=0; j--){
                byte plainbytes = bitRead(plainChar,j);
                plainteks[k]=plainbytes;
                k++;
            }
            
          }
    for (int i=0; i<80; i++){
    if(i%8==0){
      Serial.print(" "); 
    }
    if(i%32==0){
      Serial.print("\n");
    }
    Serial.print(plainteks[i]);
  }
  Serial.print('\n');
  }

void inisial(){
  Serial.print("Tahap Inisialisasi........\n");
  delay(1000);
  for(int i=0; i<80; i++){
    S[i]=kunci[i];
  }
  for(int i=80; i<93; i++){
    S[i]=0;
  }
  int k=0;
  for(int i=93; i<173; i++){
    S[i]=IV[k];
    k++;
  }
  for(int i=173; i<177; i++){
    S[i]=0;
  }
  for(int i=285; i<288; i++){
    S[i]=1;
  }
  
  for (int i=0;i<288;i++){
    if(i%8==0){
      Serial.print(" "); 
    }
    if(i%32==0){
      Serial.print("\n");
    }
    Serial.print(S[i]);
  }

    
  Serial.print("\n");
  Serial.print("Tahap inisialisasi selesai\n");

}


void persiapan(){
  Serial.print("Tahap persiapan (warm up)........\n");
  delay(1000);
  for(int i=0;i<1152;i++){
    int t1,t2,t3;
  
    t1=S[65]^S[90]&S[91]^S[92]^S[170];
    t2=S[161]^S[174]&S[175]^S[176]^S[263];
    t3=S[242]^S[285]&S[286]^S[287]^S[68];
    
    for(int i=92;i>0;i--)
        {S[i]=S[i-1];}
  
    for(int i=176;i>93;i--)
        {S[i]=S[i-1];}
  
    for(int i=287;i>177;i--)
        {S[i]=S[i-1];}
  
        S[0]=t3;
        S[93]=t1;
        S[177]=t2;
  }
  
 for (int i=0;i<288;i++){
    if(i%8==0){
      Serial.print(" "); 
    }
    if(i%32==0){
      Serial.print("\n");
    }
    Serial.print(S[i]);
  }
  Serial.print("\n");
  Serial.print("Tahap persiapan selesai\n");
}

void bangkit(){
  Serial.print("Tahap pembangkitan kunci........\n");
  unsigned int z[80];  //panjang Kunci
  for(int i=0;i<80;i++){
        t1=S[65]^S[92];
        t2=S[161]^S[176];
        t3=S[242]^S[287];
        
        z[i]=t1^t2^t3;
        t1=t1^S[90]&S[91]^S[170];
        t2=t2^S[174]&S[175]^S[263];
        t3=t3^S[285]&S[286]^S[68];
        
        for(int i=92;i>0;i--)
          {S[i]=S[i-1];}  
        for(int i=176;i>93;i--)
          {S[i]=S[i-1];}
        for(int i=287;i>177;i--)
          {S[i]=S[i-1];}
        S[0]=t3;
        S[93]=t1;
        S[177]=t2;
         if(i%8==0){
            Serial.print(" "); 
          } 
         if(i%32==0){
            Serial.print("\n");
         }
          Serial.print(z[i]);
          
    }
  
  Serial.print("\n");
  Serial.print("Tahap pembangkitan selesai\n");
}
